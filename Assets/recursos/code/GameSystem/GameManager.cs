using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    
    public Transform[] Entities;
    private static Transform p;
    public static Transform GetTransform(){return p;}
    public static GameManager GetComponent(){return p.GetComponent<GameManager>();}
    public enum CoinBlockBehaviour{Coin,MultiCoin,Mushroom,MushroomLife,Star}



    void Awake(){
        p=this.transform;
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public static Transform SpawnEntity(int ID, Vector3 pos){
            var entity=Instantiate(GetComponent().Entities[ID],pos,Quaternion.identity) as Transform;
            return entity;
    }
}
