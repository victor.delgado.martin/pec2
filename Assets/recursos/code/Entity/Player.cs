using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public SpriteRenderer renderer;
    public float Speed = 1;
    public float JumpSpeed = 2;
    public float Gravity = 1;
    private float velocity, fall, verticalVelocity;
    private float horizontalDirection = 0;
    public float SpeedUp = 4;
    public Sprite[] Idle,walk;
    public Sprite[] BigIdle,Bigwalk;
    private Sprite[] CurrentAnim;
    private int AnimState=-1;
    public bool IsBig=false;
    private float animclock;
    private int currentSprite;
    private static Transform p;
    public static Transform GetTransform(){return p;}

    private bool IsGround,Jump;


    void Start()
    {
        IsGround=true;
        p=this.transform;
        SetAnim(0,IsBig); 
       
    }

    // Update is called once per frame
    void Update()
    {

         var pos = transform.position;
        if(Input.GetKey(KeyCode.Space)&&IsGround){
            verticalVelocity -= JumpSpeed;
            Jump=true;
            IsGround=false;
        }
        if(Input.GetKey(KeyCode.A)){
            horizontalDirection=-1;
        }else if (Input.GetKey(KeyCode.D)){
            horizontalDirection=1;
        }else horizontalDirection=0;


        
       
        //colisiones
        RaycastHit hit;
        var o = 0.5f;
        if(IsBig){o=1;}
        var Origin = pos + new Vector3(0,o,0);
        if(Physics.Raycast(Origin,Vector3.right*horizontalDirection, out hit,0.4f)){
            if(hit.transform.tag == "Solid"){
                    horizontalDirection=0;
                    velocity = 0;
            }
            if(hit.transform.tag == "enemy"){
                Debug.Log("enemy");
            }
        }

    
        var Origin2 = pos + new Vector3(0,1,0);
        var o2 = 1f;

        if(Mathf.Sign(verticalVelocity)<=0){
            Origin2=pos+new Vector3(0,0.5f,0);
            o2=0.5f;
        }
        verticalVelocity+=Time.deltaTime*Gravity;
        pos.y-=verticalVelocity*Time.deltaTime;
        if(Physics.Raycast(Origin2-new Vector3(0.3f,0),Vector3.down*Mathf.Sign(verticalVelocity),out hit,o2)||Physics.Raycast(Origin2+new Vector3(0.3f,0),Vector3.down*Mathf.Sign(verticalVelocity),out hit,o2)){

        if(hit.transform.tag=="Solid"){
            if(Mathf.Sign(verticalVelocity)>=0){
                if(!IsGround){

                IsGround=true;
                Jump=false;
                pos.y=hit.transform.position.y+0.5f;
                }else{
                pos.y=hit.transform.position.y+0.5f;
                verticalVelocity=0;
                }
                }else{
                    if(hit.transform.GetComponent<CoinBlock>()!=null){
                        hit.transform.SendMessage("Act");
                    }
                pos.y=hit.transform.position.y-(o+1f);
                verticalVelocity=0;

                }
        }
            if(hit.transform.tag == "Enemy"){
                Debug.Log("enemy");
                Debug.Log("muerte");
              
                SceneManager.LoadScene("FinalGame");
            }
    }else{
    IsGround=false;
    }
        
/*
        if(Physics.Raycast(Origin2,Vector3.down, out hit,0.1f)){//hay suelo
            if(hit.transform.tag == "Solid"){
                   fall=0;
                    IsGround=true;
            }
        }else{//no hay suelo
            fall =-1;
            IsGround=false;
        }
*/

        //Colisiones
        if(verticalVelocity>=15){verticalVelocity=15;}
        velocity=Mathf.MoveTowards(velocity,horizontalDirection,SpeedUp*Time.deltaTime);
        pos.x += (Speed*velocity)*Time.deltaTime;
        transform.position=pos;

        if(transform.position.y<-5){
            Debug.Log(transform.position.y);
            Debug.Log("muerte");
              
             SceneManager.LoadScene("FinalGame");
    
        }


        velocity=Mathf.Lerp(velocity,horizontalDirection,Speed*Time.deltaTime);
        

        pos.x += ((Speed*velocity))*Time.deltaTime;
        pos.y += (Gravity*fall)*Time.deltaTime;
   

        if(horizontalDirection==1){
            renderer.flipX = false;
        }else
        if(horizontalDirection==-1){
            renderer.flipX = true;
        }
       

        transform.position=pos;

        if(horizontalDirection!=0){
            SetAnim(1,IsBig);
        }else {
            SetAnim(0,IsBig);
        }
        animclock+=Time.deltaTime*(Mathf.Abs(velocity))*(Speed*2);

        if(animclock>=1){
            currentSprite+=1;
            animclock=0;
        }

        if(currentSprite>=CurrentAnim.Length){
                currentSprite=0;
        }
        renderer.sprite= CurrentAnim[currentSprite];
    }

    void SetAnim(int state, bool big){
        if(state!=AnimState){
            currentSprite=0;
            animclock=0;
            if(state==0&&!big){CurrentAnim=Idle;}
            if(state==0&&big){CurrentAnim=BigIdle;}

            if(state==1&&!big){CurrentAnim=walk;}
            if(state==1&&big){CurrentAnim=Bigwalk;}
            AnimState = state;
        }
    }

   

}
