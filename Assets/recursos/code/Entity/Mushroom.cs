using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mushroom : MonoBehaviour
{
    private Vector3 startPos;
    private bool init, hpos, IsGround;
    private int Horizontal;
    private float verticalVelocity;
    public bool IsFlower;

    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if(!init){//no se ha activado completamente el homgo
            transform.position=Vector3.MoveTowards(transform.position, startPos+new Vector3(0,1),2*Time.deltaTime);
            if(transform.position==startPos+new Vector3(0,1)){
                init=true;
            }
        }else{//aqui se ha activado el hongo
        var pos = transform.position;
        if(!hpos){
            var playerPosX = Player.GetTransform().position.x;
            if(playerPosX<transform.position.x){
                Horizontal=1;
            }else{
                Horizontal=-1;
            }
            hpos=true;
        }
            RaycastHit hit;
            RaycastHit hit2;
            if(Physics.Raycast(transform.position,Vector3.right*Horizontal, out hit,0.4f)){
                if(hit.transform.tag == "Solid"){
                        Horizontal=-Horizontal;
                }
            }
            pos.y+=verticalVelocity*Time.deltaTime;
        verticalVelocity+=Time.deltaTime*32;
        if(Physics.Raycast(pos-new Vector3(0.3f,0),Vector3.down,out hit,0.5f)||Physics.Raycast(pos+new Vector3(0.3f,0),Vector3.down,out hit,0.5f)){
            if(hit.transform.tag=="Solid"){
            if(IsGround){
            pos.y=hit.transform.position.y+1;
             verticalVelocity=0;
        }else{
            IsGround=true;
        }
        }
        }
        pos.x += Horizontal*Time.deltaTime*2;
        pos.y -= verticalVelocity*Time.deltaTime*2;
        transform.position = pos;
        }
        Debug.Log(verticalVelocity);
    }
}
