using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Goomba:MonoBehaviour{
public bool IsKoopaTroopa;
public float AnimSpeed=2;
public Sprite[] Anim,Anim2;
public Sprite Die;
public int Horizontal=-1;
private Vector3 startPos;
private float VerticalVelocity;
private bool hpos, IsGround,hhh,isview=false;
private Transform DownBlock;
private float animclock;
private int CurrentSprite;
public Sprite[] CurrentAnim;
public bool kill=false,kill2=false,kill3=false;
void Start(){
CurrentAnim=Anim;
}
public void Jump(){
VerticalVelocity=-10;
IsGround=false;
hpos=false;
var pos=transform.position;
if(!hpos){
var playerPosX=Player.GetTransform().position.x;
if(playerPosX<transform.position.x){
Horizontal=1;
}else{
Horizontal=-1;
}
hpos=true;
}
GetComponent<SpriteRenderer>().flipY=true;
Kill();
kill3=true;
}
public void Kill(){

if(kill2||kill3){
kill2=false;
CurrentAnim=Anim2;
CurrentSprite=0;
kill=false;
hhh=false;
}
if(kill3){
kill2=true;
kill=true;
GetComponent<SpriteRenderer>().sprite=Die;
}
if(kill){
kill2=true;
GetComponent<SpriteRenderer>().sprite=Die;
}

kill=true;
if(!GetComponent<SpriteRenderer>().flipY){
if(!IsKoopaTroopa){
GetComponent<SpriteRenderer>().sprite=Die;
Destroy(gameObject,1);
}else{
CurrentAnim=Anim2;
CurrentSprite=0;
}
}else{
if(IsKoopaTroopa){
GetComponent<SpriteRenderer>().sprite=Die;
}
Destroy(gameObject,2);
}
}
void Update(){

var Distance=Vector3.Distance(new Vector3(transform.position.x,0),new Vector3(Player.GetTransform().position.x,0));
if(!GetComponent<Renderer>().isVisible&&Player.GetTransform().position.x>=transform.position.x){
Destroy(gameObject);
}
var render=GetComponent<SpriteRenderer>();
if(Distance<=10||isview){
isview=true;
animclock+=Time.deltaTime*AnimSpeed;
if(animclock>=1){
CurrentSprite+=1;
animclock=0;
}
if(CurrentSprite>=CurrentAnim.Length){
CurrentSprite=0;
if(IsKoopaTroopa&&kill&&!kill2){
CurrentAnim=Anim;
kill=false;
if(!GetComponent<SpriteRenderer>().flipY){
Horizontal=-1;
}else{
Horizontal=1;
}
}
}
var pos=transform.position;
RaycastHit hit;
if(!kill3){
if(Physics.Raycast(transform.position,Vector3.right*Horizontal,out hit,0.4f)){
if(hit.transform.tag=="Solid"||hit.transform.tag=="Enemy"&&!kill2){
if(!kill||kill2){
Horizontal=-Horizontal;
if(IsKoopaTroopa&&kill2){

}
}
}
}
}
if(kill&&!hhh){
if(!GetComponent<SpriteRenderer>().flipY){
Horizontal=0;
}
}
if(!kill&&!kill2||IsKoopaTroopa&&!kill2){
if(!kill3){
render.sprite=CurrentAnim[CurrentSprite];
}
}
VerticalVelocity+=Time.deltaTime*32;
if(!kill3){
if(Physics.Raycast(pos-new Vector3(0.3f,0),Vector3.down,out hit,0.5f)||Physics.Raycast(pos+new Vector3(0.3f,0),Vector3.down,out hit,0.5f)){
if(hit.transform.tag=="Solid"){
if(IsGround&&!kill||kill2&&IsGround){
pos.y=hit.transform.position.y+1;
VerticalVelocity=0;
}else{
IsGround=true;
if(kill&&!GetComponent<SpriteRenderer>().flipY){
pos.y=hit.transform.position.y+1;
VerticalVelocity=0;
}
}
}
}
if(Physics.Raycast(pos,Vector3.down,out hit,0.75f)){
if(hit.transform.tag=="Solid"){
if(DownBlock!=null){
if(DownBlock!=hit.transform){DownBlock.SendMessage("SetUpObjectNull");}
}

}
}
}
var speed=1.5f;
if(kill2&&!kill3){
speed=6;
}
if(kill2&&!hhh){
hhh=true;
if(Player.GetTransform().position.x<transform.position.x){
Horizontal=1;
}else{
Horizontal=-1;
}
}
if(Horizontal==1){//Derecha
render.flipX=true;
}else 
if(Horizontal==-1){
render.flipX=false;
}
pos.x+=Horizontal*Time.deltaTime*speed;
pos.y-=VerticalVelocity*Time.deltaTime;
transform.position=pos;
}

}
void OnTriggerEnter(Collider c){
if(c.GetComponent<Goomba>()!=null){
if(c.GetComponent<Goomba>().IsKoopaTroopa&& c.GetComponent<Goomba>().kill2&&!kill){
Jump();
}
}
}
}