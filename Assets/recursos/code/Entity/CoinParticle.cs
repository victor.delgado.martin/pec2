using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinParticle:MonoBehaviour{
public float AnimSpeed=2;
public Sprite[] Anim;
public float VerticalVelocity=4;
private Vector3 startPos,pos;
private bool init;
private float animclock;
private int CurrentSprite;
IEnumerator Start(){
startPos=transform.position;
yield return new WaitForSeconds(0.6f);

Destroy(gameObject);
}
void Update(){
pos=transform.position;
var render=GetComponent<SpriteRenderer>();
animclock+=Time.deltaTime*AnimSpeed;
if(animclock>=1){
CurrentSprite+=1;
render.sortingOrder=0;
animclock=0;
}
if(CurrentSprite>=Anim.Length){
CurrentSprite=0;
}
render.sprite=Anim[CurrentSprite];
VerticalVelocity-=Time.deltaTime*8;
pos.y+=VerticalVelocity*Time.deltaTime*8;
transform.position=pos;
/*
if(!init){
transform.position=Vector3.MoveTowards(transform.position,startPos+new Vector3(0,3.5f),16*Time.deltaTime);
if(transform.position==startPos+new Vector3(0,3.5f)){
init=true;
}
}
if(init){
transform.position=Vector3.MoveTowards(transform.position,startPos+new Vector3(0,1.5f),16*Time.deltaTime);
if(transform.position==startPos+new Vector3(0,1.5f)){
Destroy(this.gameObject);
}
}*/
}
}
